/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 18:14:41 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/06 09:50:39 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t		len;
	size_t		i;
	char		*p;
	char		*pp;

	if (s == NULL || f == NULL)
		return (NULL);
	len = ft_strlen(s);
	if ((p = malloc(len + 1)) == NULL)
		return (NULL);
	pp = p;
	i = 0;
	while (i < len)
	{
		*p++ = f(i, *s++);
		i++;
	}
	*p = '\0';
	return (pp);
}
