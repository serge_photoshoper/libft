/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:31:58 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:16:54 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a copy of
** ’s1’ with the characters specified in ’set’ removed
** from the beginning and the end of the string.
** Parameters : #1. The string to be trimmed.
**              #2. The reference set of characters to trim.
** Return value : The trimmed string. NULL if the allocation fails.
*/

char	*ft_strtrim(char const *s1, char const *set)
{
	char const	*p;
	char		*trim;

	if (s1 == NULL)
		return (NULL);
	if (set == NULL || *s1 == '\0')
		return (ft_strdup(s1));
	p = s1 + ft_strlen(s1) - 1;
	while ((*s1 != '\0') && (ft_strchr(set, *s1) != NULL))
		s1++;
	if (*s1 == '\0')
		return (ft_strdup(s1));
	while ((p != s1) && (ft_strchr(set, *p) != NULL))
		p--;
	if ((trim = (char *)malloc(p - s1 + 2)) == NULL)
		return (NULL);
	ft_memmove(trim, s1, p - s1 + 1);
	*(trim + (p - s1 + 1)) = '\0';
	return (trim);
}
