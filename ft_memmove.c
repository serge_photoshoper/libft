/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:24:48 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:10:10 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	ptrdiff_t		p;
	unsigned char	*s;
	unsigned char	*d;

	p = dest - src;
	if (n == 0 || p == 0)
		return (dest);
	if (p > 0)
	{
		s = (unsigned char *)src + n - 1;
		d = (unsigned char *)dest + n - 1;
		while (n-- != 0)
			*d-- = *s--;
	}
	else
	{
		s = (unsigned char *)src;
		d = (unsigned char *)dest;
		while (n-- != 0)
			*d++ = *s++;
	}
	return (dest);
}
