/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/02 19:29:42 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/06 09:29:51 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** it is guaranteed that only long long is bigger than int in C99 standart
*/

static	void	convert(long long nn, int fd)
{
	if (nn / 10 == 0)
		ft_putchar_fd(nn + '0', fd);
	else
	{
		convert(nn / 10, fd);
		ft_putchar_fd(nn % 10 + '0', fd);
	}
}

void			ft_putnbr_fd(int n, int fd)
{
	long long int	nn;

	if (fd < 0)
		return ;
	nn = n;
	if (nn < 0)
	{
		ft_putchar_fd('-', fd);
		nn = (-1) * nn;
	}
	convert(nn, fd);
}
