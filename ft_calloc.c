/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:15:21 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/05 00:10:40 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** in linux it returns either NULL or valid pointe
** in the case of size = 0 or nmemb = 0
** in macos it returns NULL only on error (and sets errno to ENOMEM
*/

void	*ft_calloc(size_t nmemb, size_t size)
{
	void	*p;

	if ((p = malloc(size * nmemb)) == NULL)
		return (NULL);
	ft_memset(p, 0, size * nmemb);
	return (p);
}
