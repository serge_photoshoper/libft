/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:26:46 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:12:08 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	unsigned char	sym;
	unsigned char	*str;

	sym = (unsigned char)c;
	str = (unsigned char *)s;
	while (*str != '\0')
	{
		if (*str == sym)
			return ((char *)str);
		else
			str++;
	}
	if (sym == '\0')
		return ((char *)str);
	else
		return (NULL);
}
