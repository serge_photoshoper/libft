/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:15:58 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:05:11 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** This is how this function is realized in glibc with locale support
** return ((*__ctype_b_loc())[c] & (unsigned short int)_ISalpha);
** return ((*__ctype_b_loc())[c] & 0x400);
*/

static	int		ft_islower(int c)
{
	if (c == EOF)
		return (0);
	if (c >= 'a' && c <= 'z')
		return (1);
	return (0);
}

static	int		ft_isupper(int c)
{
	if (c == EOF)
		return (0);
	if (c >= 'A' && c <= 'Z')
		return (1);
	return (0);
}

int				ft_isalpha(int c)
{
	if (c == EOF)
		return (0);
	return (ft_isupper(c) || ft_islower(c));
}
