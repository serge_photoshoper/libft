/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:29:35 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:15:38 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Original strlcpy & strlcat crashes with NULL arguments!
** So, I make the same. No protection from NULL arguments!
** But I add the protection from src = NULL as Alexander Shust
*/

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	size_t	len;

	if (src == NULL)
		return (0);
	len = ft_strlen(src);
	if (size == 0)
		return (len);
	ft_memmove(dst, src, ((len > size - 1) ? size - 1 : len));
	*(dst + ((len > size - 1) ? size - 1 : len)) = '\0';
	return (len);
}
