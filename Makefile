#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2020/04/30 12:41:41 by gejeanet          #+#    #+#             *#
#*   Updated: 2020/05/06 13:00:33 by gejeanet         ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

.PHONY: all clean fclean re bonus

SRC = ft_memset.c ft_bzero.c ft_memcpy.c ft_memccpy.c ft_memmove.c ft_memchr.c \
		ft_memcmp.c ft_strlen.c ft_strlcpy.c ft_strlcat.c ft_strchr.c \
		ft_strrchr.c ft_strnstr.c ft_strncmp.c ft_atoi.c ft_isalpha.c \
		ft_isascii.c ft_isalnum.c ft_isdigit.c ft_isprint.c \
		ft_toupper.c ft_tolower.c \
		ft_calloc.c ft_strdup.c ft_substr.c ft_strjoin.c ft_strtrim.c \
		ft_split.c ft_itoa.c ft_strmapi.c \
		ft_putchar_fd.c ft_putstr_fd.c ft_putendl_fd.c ft_putnbr_fd.c

BONUS_SRC = ft_lstnew.c ft_lstdelone.c \
			ft_lstadd_front.c ft_lstsize.c ft_lstlast.c ft_lstadd_back.c \
			ft_lstclear.c ft_lstiter.c ft_lstmap.c
CC = gcc 
NAME = libft.a
CFLAGS = -Wall -Werror -Wextra -I.

OBJ =	$(SRC:.c=.o)
BONUS_OBJ =	$(BONUS_SRC:.c=.o)

%.o: %.c libft.h
	gcc -c -o $@ $< $(CFLAGS)

$(NAME): $(OBJ)
	ar rc $@ $?
	ranlib $@

bonus: $(BONUS_OBJ)
	ar rc $(NAME) $?
	ranlib $(NAME)

all: $(NAME) bonus

clean:
	/bin/rm -f $(OBJ) $(BONUS_OBJ)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
