/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:32:14 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:17:01 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a substring
** from the string ’s’.
** The substring begins at index ’start’ and is of
** maximum size ’len’.
** Return value : The substring. NULL if the allocations fails.
**
** If start > strlen(s) then we can return NULL or empty string.
** I prefer to return (ft_strdup("") (near line 37-38)
*/

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*p;
	char	*pp;
	size_t	src_len;
	size_t	dup_size;

	if (s == NULL)
		return (NULL);
	src_len = ft_strlen(s);
	if (src_len < (size_t)start)
		return (ft_strdup(""));
	dup_size = src_len - (size_t)start;
	dup_size = ((dup_size > len) ? len : dup_size);
	if ((p = (char *)malloc(dup_size + 1)) == NULL)
		return (NULL);
	pp = p;
	s = s + start;
	while (dup_size != 0)
	{
		*p++ = *s++;
		dup_size--;
	}
	*p = '\0';
	return (pp);
}
