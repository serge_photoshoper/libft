/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:30:24 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:16:16 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	while (n != 0 && *s1 != '\0' && *s2 != '\0')
	{
		if (*s1 != *s2)
			break ;
		else
		{
			n--;
			s1++;
			s2++;
		}
	}
	if (n == 0 || (*s1 == '\0' && *s2 == '\0'))
		return (0);
	else
		return ((int)(unsigned char)(*s1) - (int)(unsigned char)(*s2));
}
