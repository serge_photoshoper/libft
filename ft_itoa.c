/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 00:33:07 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:06:35 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** it is guaranteed that only long long is bigger than int in C99 standart
*/

static void	convert(char **s, long long int n)
{
	if (n / 10 == 0)
		*(*s)++ = ((char)n) + '0';
	else
	{
		convert(s, n / 10);
		*(*s)++ = ((char)(n % 10)) + '0';
	}
}

char		*ft_itoa(int n)
{
	char			*str;
	char			*ostr;
	long long int	nn;
	int				number_of_digits;

	nn = n;
	number_of_digits = (n < 0) ? 2 : 1;
	while (n / 10 != 0)
	{
		n = n / 10;
		number_of_digits++;
	}
	if ((str = malloc(number_of_digits + 1)) == NULL)
		return (NULL);
	ostr = str;
	if (nn < 0)
	{
		*str++ = '-';
		nn = (-1) * nn;
	}
	convert(&str, nn);
	*str = '\0';
	return (ostr);
}
