/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:10:46 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 22:42:02 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_skip_to_digit(char *p, int *sign)
{
	*sign = 1;
	while (*p == ' ' || *p == '\f' || *p == '\n' || \
			*p == '\r' || *p == '\t' || *p == '\v')
		p++;
	if (*p == '+')
	{
		*sign = 1;
		p++;
	}
	else if (*p == '-')
	{
		*sign = -1;
		p++;
	}
	return (p);
}

int			ft_atoi(const char *nptr)
{
	int			is_negative;
	long int	result;
	const char	*p_begin;
	ptrdiff_t	count;

	nptr = ft_skip_to_digit((char *)nptr, &is_negative);
	p_begin = nptr;
	while (*nptr >= '0' && *nptr <= '9')
		nptr++;
	result = 0;
	count = nptr - p_begin;
	while (count-- != 0)
	{
		if (result > LONG_MAX / 10)
			break ;
		result = result * 10;
		if (result > LONG_MAX - (int)(*p_begin - '0'))
			break ;
		result = result + (*(p_begin++) - '0');
	}
	if (is_negative == 1 && *p_begin >= '0' && *p_begin <= '9')
		return ((int)LONG_MAX);
	else if (is_negative == -1 && *p_begin >= '0' && *p_begin <= '9')
		return ((int)LONG_MIN);
	return (result * is_negative);
}
