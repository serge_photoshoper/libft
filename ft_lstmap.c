/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/03 22:04:18 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/06 10:03:25 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Iterates the list ’lst’ and applies the function
** ’f’ to the content of each element. Creates a new
** list resulting of the successive applications of
** the function ’f’. The ’del’ function is used to
** delete the content of an element if needed.
**
** Parameters : #1. The adress of a pointer to an element.
**              #2. The adress of the function used to iterate on the list.
**              #3. The adress of the function used to delete the
**                  content of an element if needed.
** Return value : The new list. NULL if the allocation fails.
**
** Some explanation from stackoverflow forum :
**
** 1. lstmap creates a copy of the original list,
**    do not change/free the original one.
** 2. (*del) is only useful when a malloc fails.
** 3. Do not free the original.
** 4. The content of each element in your new list should be the result of (*f),
**    with the original element's content as a parameter.
** 5. If a malloc fails, you need to free each already allocated content
**    and element of your new list (you already wrote a function for that ;)).
** 6. Even if (*f) returns NULL, do not skip an element in your list.
*/

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*p;
	t_list	*new_lst;
	void	*content_modifyed;

	if (lst == NULL || f == NULL || del == NULL)
		return (NULL);
	new_lst = NULL;
	while (lst != NULL)
	{
		content_modifyed = f(lst->content);
		if ((p = ft_lstnew(content_modifyed)) == NULL)
		{
			del(content_modifyed);
			ft_lstclear(&new_lst, del);
			return (new_lst);
		}
		ft_lstadd_back(&new_lst, p);
		lst = lst->next;
	}
	return (new_lst);
}
