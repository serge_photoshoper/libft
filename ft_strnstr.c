/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:31:29 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:16:24 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	l_len;
	size_t	b_len;
	size_t	n;
	size_t	i;

	if ((l_len = ft_strlen(little)) == 0)
		return ((char *)big);
	b_len = ft_strlen(big);
	n = (b_len > len) ? len : b_len;
	while (n != 0 && n >= l_len)
	{
		i = 0;
		while (i < l_len && i < n)
		{
			if (*(unsigned char *)(big + i) != *(unsigned char *)(little + i))
				break ;
			i++;
		}
		if (i == l_len)
			return ((char *)big);
		big++;
		n--;
	}
	return (NULL);
}
