/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:24:32 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:09:52 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char *p;
	unsigned char *s;

	if (n != 0 && dest == NULL && src == NULL)
		return (dest);
	if (dest == src)
		return (dest);
	p = (unsigned char *)dest;
	s = (unsigned char *)src;
	while (n-- != 0)
		*p++ = *s++;
	return (dest);
}
