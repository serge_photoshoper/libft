/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 11:34:05 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:17:22 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** in man : return UNCHANGED if conversion is not possible
** in fact: return the first byte of converted to unsigned char
*/

int	ft_toupper(int c)
{
	if (c == EOF)
		return (EOF);
	if ((unsigned char)c >= 'a' && (unsigned char)c <= 'z')
		return (c - 32);
	else
		return ((c > 0) ? c : (int)((unsigned int)c & 0xFF));
}
