/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:25:20 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:11:53 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*ft_strdup_c(const char *s, char c)
{
	void		*p;
	const char	*ss;
	size_t		len;

	len = 0;
	ss = s;
	while (*ss != c && *ss != '\0')
	{
		ss++;
		len++;
	}
	if ((p = malloc(len + 1)) == NULL)
		return (NULL);
	ft_memmove(p, s, len);
	*((unsigned char *)p + len) = '\0';
	return ((char *)p);
}

static size_t	word_count(const char *s, char c)
{
	size_t	i;

	i = 0;
	while (*s != '\0')
	{
		while (*s == c)
			s++;
		if (*s != '\0')
			i++;
		while (*s != c && *s != '\0')
			s++;
		if (*s == c)
			continue ;
		else
			break ;
	}
	return (i);
}

/*
** create array of pointers to splitted words
** and NULL-terminate them
*/

static void		*prepare_to_split(char ***ar, const char *s, char c)
{
	size_t	i;

	i = word_count(s, c);
	if ((*ar = (char **)malloc((i + 1) * sizeof(*ar))) == NULL)
		return (NULL);
	i = 0;
	while (*s != '\0')
	{
		while (*s == c)
			s++;
		if (*s != '\0')
		{
			(*ar)[i] = (char *)s;
			i++;
		}
		while (*s != c && *s != '\0')
			s++;
		if (*s == c)
			continue ;
		else
			break ;
	}
	(*ar)[i] = NULL;
	return ((void *)(*ar));
}

/*
** Allocates (with malloc(3)) and returns an array
** of strings obtained by splitting ’s’ using the
** character ’c’ as a delimiter. The array must be
** ended by a NULL pointer.
**
** Parameters : #1. The string to be split.
**              #2. The delimiter character.
** Return value :
** The array of new strings resulting from the split.
** NULL if the allocation fails.
**
** char **ar - NULL-terminated array of splitted words
**             memory for it is allocated in prepare_to_split()
**
** Memory for splitted words is allocated by ft_strdup_c()
*/

char			**ft_split(char const *s, char c)
{
	char	**ar;
	size_t	i;

	if (s == NULL)
		return (NULL);
	if (prepare_to_split(&ar, s, c) == NULL)
		return (NULL);
	i = 0;
	while (ar[i] != NULL)
	{
		ar[i] = ft_strdup_c(ar[i], c);
		if (ar[i] == NULL)
		{
			while (i > 0)
			{
				i--;
				free(ar[i]);
			}
			free(ar);
			return (NULL);
		}
		i++;
	}
	return (ar);
}
