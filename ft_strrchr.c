/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:31:50 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:16:44 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	unsigned char	sym;
	unsigned char	*str;
	unsigned char	*p;

	sym = (unsigned char)c;
	str = (unsigned char *)s;
	p = NULL;
	while (*str != '\0')
	{
		if (*str == sym)
		{
			p = str;
			str++;
		}
		else
			str++;
	}
	if (sym == '\0')
		return ((char *)str);
	else
		return ((char *)p);
}
