/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:27:56 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:15:22 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Original strlcpy & strlcat crashes with NULL arguments!
** So, I make the same. No protection from NULL arguments!
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	len_src;
	size_t	len_dst;
	size_t	b_num;

	len_src = ft_strlen(src);
	len_dst = ft_strlen(dst);
	if (size == 0)
		return (size + len_src);
	if (len_dst > size - 1)
		return (size + len_src);
	if (len_dst == size - 1)
		return (len_dst + len_src);
	if (len_src > (size - 1 - len_dst))
		b_num = size - 1 - len_dst;
	else
		b_num = len_src;
	ft_memmove(dst + len_dst, src, b_num);
	*(dst + len_dst + b_num) = '\0';
	return (len_src + len_dst);
}
