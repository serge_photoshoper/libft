/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/03 14:53:36 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/06 09:55:33 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Deletes and frees the given element and every
** successor of that element, using the function ’del’
** and free(3).
** Finally, the pointer to the list must be set to
** NULL.
**
** Parameters : #1. The adress of a pointer to an element.
**              #2. The adress of the function used to delete the
**                  content of the element.
*/

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*p;
	t_list	*p_tmp;

	if (lst == NULL || *lst == NULL || del == NULL)
		return ;
	p = *lst;
	while (p != NULL)
	{
		p_tmp = p->next;
		del(p->content);
		free(p);
		p = p_tmp;
	}
	*lst = NULL;
}
