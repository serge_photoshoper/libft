/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:27:25 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/05 00:10:18 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	void	*p;
	size_t	len;

	len = ft_strlen(s);
	if ((p = malloc(len + 1)) == NULL)
		return (NULL);
	ft_memmove(p, s, len);
	*((unsigned char *)p + len) = '\0';
	return ((char *)p);
}
