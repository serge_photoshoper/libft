/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:20:30 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:09:21 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char symbol;
	unsigned char *src;

	if (n == 0)
		return (NULL);
	symbol = (unsigned char)c;
	src = (unsigned char *)s;
	while (n-- != 0)
	{
		if (*src == symbol)
			return ((void *)src);
		else
			src++;
	}
	return (NULL);
}
