/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gejeanet <gejeanet@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/30 11:19:53 by gejeanet          #+#    #+#             */
/*   Updated: 2020/05/04 23:08:58 by gejeanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char *p;
	unsigned char *s;

	p = (unsigned char *)dest;
	s = (unsigned char *)src;
	while (n-- != 0)
	{
		if (*s == (unsigned char)c)
		{
			*p++ = *s;
			return (p);
		}
		else
		{
			*p++ = *s++;
			continue ;
		}
	}
	return (NULL);
}
